-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 19-05-2024 a las 05:07:04
-- Versión del servidor: 8.2.0
-- Versión de PHP: 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE IF NOT EXISTS `materias` (
  `id_materias` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_materias`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id_materias`, `nombre`) VALUES
(3, 'ingles 3'),
(8, 'ingles 3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas1`
--

DROP TABLE IF EXISTS `tareas1`;
CREATE TABLE IF NOT EXISTS `tareas1` (
  `id_tareas` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fecha_entrega` date NOT NULL,
  `hora_entrega` time NOT NULL,
  PRIMARY KEY (`id_tareas`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tareas1`
--

INSERT INTO `tareas1` (`id_tareas`, `titulo`, `fecha_entrega`, `hora_entrega`) VALUES
(4, 'sdfsdfert', '2024-05-07', '16:42:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuarios` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `id_rol` tinyint NOT NULL,
  PRIMARY KEY (`id_usuarios`),
  UNIQUE KEY `correo_UNIQUE` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuarios`, `nombre`, `correo`, `password`, `id_rol`) VALUES
(2, 'rolando', 'rolando@ho.com', '81dc9bdb52d04dc20036dbd8313ed055', 0),
(5, 'sebastian fregoso', 'sabas@menso.com', '81dc9bdb52d04dc20036dbd8313ed055', 1),
(6, 'antonio gonzales', 'tony@panzona.com', '81dc9bdb52d04dc20036dbd8313ed055', 1),
(7, 'alberto', 'albert@y.com', '81dc9bdb52d04dc20036dbd8313ed055', 1),
(8, 'filomena', 'fi@ho.com', '81dc9bdb52d04dc20036dbd8313ed055', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_materias`
--

DROP TABLE IF EXISTS `usuarios_has_materias`;
CREATE TABLE IF NOT EXISTS `usuarios_has_materias` (
  `usuarios_id_usuarios` int NOT NULL,
  `materias_id_materias` int NOT NULL,
  PRIMARY KEY (`usuarios_id_usuarios`,`materias_id_materias`),
  KEY `fk_usuarios_has_materias_materias1_idx` (`materias_id_materias`),
  KEY `fk_usuarios_has_materias_usuarios_idx` (`usuarios_id_usuarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuarios_has_materias`
--
ALTER TABLE `usuarios_has_materias`
  ADD CONSTRAINT `fk_usuarios_has_materias_materias1` FOREIGN KEY (`materias_id_materias`) REFERENCES `materias` (`id_materias`),
  ADD CONSTRAINT `fk_usuarios_has_materias_usuarios` FOREIGN KEY (`usuarios_id_usuarios`) REFERENCES `usuarios` (`id_usuarios`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
